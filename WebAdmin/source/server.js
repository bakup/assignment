/*var http = require('http');
 

var server = http.createServer(function(request, response){   
    response.writeHead(200, {
        "Context-type" : "text/plain"
    });
     
    // Show th�ng tin
    response.write('Your URL is ' + request.url);
     
    
    response.end();
});
 
server.listen(3000, function(){
    console.log('Connected Successfull!');
});
*/

var express = require('express');
var path = require('path');

var app = express();
app.use(express.static(path.join(__dirname, '/src')));
app.use(express.static(path.join(__dirname, '/dist')));

app.listen(3000, function() {
	console.log('started listen port', 3000);
});
