# assignment
** Chạy trực tiếp **
1. Chay file assignment_sql script de tao database
2. java -jar restaurant-0.0.1-SNAPSHOT
==> OK

** Build và Run để DEV API yêu cầu cần cài maven, nodejs **
1. Build API: -mvn clean install
2. Run API: -mvn spring-boot:run

** Build và Run để DEV FRONT-END
1. -npm install
2. -npm run serve

** ADD ITEM **
JSON POST
{
	"name": "Mon an ten la abcdef",
	"imageUrl": "/folders/images/imageabc.jpg",
	"description": "description examples",
	"price": 100000
}

Response:
 {	
	"id": 55
	"name": "Mon an ten la abcdef",
	"imageUrl": "/folders/images/imageabc.jpg",
	"description": "description examples",
	"price": 100000
}


** ADD ORDER **

JSON POST
{
	"tableId": 10,
	"customer": "Hung",
	"items": [5,6,7],   # item id list, item phai dc insert trc
	"remark": "Add new order test"
}

Response:
{
    "id": 4,
    "orderDate": "2018-12-20",
    "amount": 300000,
    "vat": 0,
    "totalAmount": 300000,
    "discount": 0,
    "remark": "Add new order test",
    "status": "A",
    "customerName": "Hung",
    "tableId": 10,
    "orderDetails": [
        {
            "id": 1,
            "remark": "",
            "status": "A",
            "item": {
                "id": 5,
                "name": "Mon an ten la abcdef",
                "imageUrl": "/folders/images/imageabc.jpg",
                "description": "abcd",
                "price": 100000,
                "status": true
            }
        },
        {
            "id": 2,
            "remark": "",
            "status": "A",
            "item": {
                "id": 6,
                "name": "ITEM444",
                "imageUrl": "/folders/images/imageabc.jpg",
                "description": "abcd",
                "price": 100000,
                "status": true
            }
        },
        {
            "id": 3,
            "remark": "",
            "status": "",
            "item": {
                "id": 7,
                "name": "ITEM444",
                "imageUrl": "/folders/images/imageabc.jpg",
                "description": "abcd",
                "price": 100000,
                "status": true
            }
        }
    ]
}# assignment

