drop schema if exists restaurant;
CREATE SCHEMA restaurant DEFAULT CHARACTER SET utf8 ;
use restaurant;

drop table if exists employee;
create table employee(
	id bigint primary key auto_increment,
    username varchar(50) not null,
    password varchar(250) not null,
    phone varchar(50),
    email varchar(50),
    fullname varchar(100),
    gender char(1),
    dob datetime,
    role_id bigint,
    branch_id bigint,
    status char(1)
);

drop table if exists role;
create table role(
	id bigint primary key auto_increment,
    role_name varchar(100) not null,
    status varchar(1)
);

drop table if exists branch;
create table branch(
	id bigint primary key auto_increment,
    code varchar(5),
    name varchar(100),
    city_id bigint,
    district_id bigint,
    ward_id bigint,
    street varchar(250),
    address varchar(500),
    status char(1)
);

drop table if exists branch_users;
create table branch_users(
	user_id bigint,
    branch_id bigint,
    status char(1),
    start_date datetime
);

drop table if exists item;
create table item(
	id bigint primary key auto_increment,
    name varchar(250),
    image_url varchar(250),
    description varchar(500),
    status char(1)
);

drop table if exists branch_items;
create table branch_items(
	item_id bigint,
    branch_id bigint,
    price numeric(10, 2),
    status char(1)
);

alter table branch_items
add constraint fk_branch_items_branch 
foreign key (branch_id) references branch(id) on update cascade on delete restrict;

alter table branch_items
add constraint fk_branch_items_item
foreign key (item_id) references item(id) on update cascade on delete restrict;

drop table if exists orders;
create table orders(
	id bigint primary key auto_increment,
    branch_id bigint,
    user_id bigint,
    order_date datetime,
    amount numeric(15, 2),
    vat numeric(15,2),
    total_amount numeric(15,2),
    discount smallint,
    remark varchar(250),
    customer_name varchar(100),
    table_id bigint,
    status char(1)
);



drop table if exists orders_detail;
create table orders_detail(
	id bigint primary key auto_increment,
    orders_id bigint,
    item_id bigint,
    price numeric(15,2),
    remark varchar(250),
    status char(1)
);

alter table branch_users
add constraint fk_branch_users_user
foreign key (user_id) references employee(id) on update cascade on delete restrict;

alter table branch_users
add constraint fk_branch_users_branch
foreign key (branch_id) references branch(id) on update cascade on delete restrict;

alter table orders
add constraint fk_orders_branch
foreign key (branch_id) references branch(id) on delete restrict on update cascade;

alter table orders
add constraint fk_orders_user
foreign key (user_id) references employee(id) on delete restrict on update cascade;

alter table employee
add constraint fk_employee_role
foreign key (role_id) references role(id) on update cascade on delete restrict;

alter table employee
add constraint fk_employee_branch
foreign key (branch_id) references branch(id) on update cascade on delete restrict;

alter table orders_detail
add constraint fk_ordes_detail_orders
foreign key (orders_id) references orders(id) on delete restrict on update cascade;

alter table orders_detail
add constraint fk_orders_detail_item
foreign key (item_id) references item(id) on delete restrict on update cascade;


