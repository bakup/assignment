package hcmut.restaurant.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@Import(DataSourceConfig.class)
@EnableJpaRepositories({"hcmut.restaurant.entity", "hcmut.restaurant.repository"})
public class JpaConfig {
}
