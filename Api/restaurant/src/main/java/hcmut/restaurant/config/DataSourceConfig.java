package hcmut.restaurant.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:application.properties")
@EnableTransactionManagement
public class DataSourceConfig {

    @Value("${ds.url}")
    private String url;

    @Value("${ds.driver}")
    private String driver;

    @Value("${ds.schema}")
    private String schema;

    @Value("${ds.username}")
    private String username;

    @Value("${ds.password}")
    private String password;

    @Value("${jpa.showSql}")
    private String showSql;

    @Value("{jpa.generateDdl}")
    private String generateDdl;

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource ds = new DriverManagerDataSource();

        ds.setUrl(url);
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setSchema(schema);
        ds.setDriverClassName(driver);

        return ds;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        HibernateJpaVendorAdapter jpaVender = new HibernateJpaVendorAdapter();
        jpaVender.setShowSql(Boolean.valueOf(showSql));
        jpaVender.setDatabase(Database.MYSQL);
        jpaVender.setGenerateDdl(Boolean.valueOf(generateDdl));

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(jpaVender);
        factory.setDataSource(dataSource());
        factory.setPackagesToScan("hcmut.restaurant.entity");

        return factory;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return txManager;
    }
}
