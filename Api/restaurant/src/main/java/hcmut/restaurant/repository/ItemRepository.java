package hcmut.restaurant.repository;

import hcmut.restaurant.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item, Long> {

}
