package hcmut.restaurant.repository;

import hcmut.restaurant.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
