package hcmut.restaurant.service;

import hcmut.restaurant.dto.OrderDto;
import hcmut.restaurant.entity.Item;
import hcmut.restaurant.entity.Order;
import hcmut.restaurant.entity.OrderDetail;
import hcmut.restaurant.repository.ItemRepository;
import hcmut.restaurant.repository.OrderDetailRepository;
import hcmut.restaurant.repository.OrderRepository;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {
    @Autowired
    OrderRepository orderRepository;
    
    @Autowired
    OrderDetailRepository orderDetailRepository;
    
    @Autowired
    ItemRepository itemRepository;
    
    private List<OrderDetail> createOrderDetails(Order order, List<Item> items) {
        List<OrderDetail> result = new ArrayList<>();
        
        for (Item item : items) {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setOrder(order);
            orderDetail.setItem(item);
            result.add(orderDetail);
        }
        
        return result;
    }
    
    public Order addOrder(OrderDto orderInfo) {
        Order order = new Order();
        
        List<Item> items      = itemRepository.findAllById(orderInfo.getItems());
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (Item item : items) {
            BigDecimal itemPrice = item.getPrice();
            totalPrice = totalPrice.add(itemPrice);
        }
        
        order.setAmount(totalPrice);
        order.setCustomerName(orderInfo.getCustomer());
        order.setRemark(orderInfo.getRemark());
        order.setVat(orderInfo.getVat());
        order.setDiscount(orderInfo.getDiscount());
        order.setTotalAmount(totalPrice.subtract(totalPrice.multiply(order.getDiscount())));
        order.setOrderDate(LocalDate.now());
        order.setTableId(orderInfo.getTableId());
    
        Order savedOrder = orderRepository.save(order);
        List<OrderDetail> orderDetails = createOrderDetails(order, items);
        List<OrderDetail> savedOrderDetails = orderDetailRepository.saveAll(orderDetails);
        savedOrder.setOrderDetails(savedOrderDetails);
        
        return savedOrder;
    }
    
    
    public OrderDetail addOrderDetail(OrderDetail orderDetail) {
        return orderDetailRepository.save(orderDetail);
    }
}
