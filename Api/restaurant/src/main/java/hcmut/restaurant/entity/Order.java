package hcmut.restaurant.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CollectionId;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "orders")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Order implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @JsonProperty
    @Column(name = "order_date")
    private LocalDate orderDate;
    
    @JsonProperty
    @Column
    private BigDecimal amount = BigDecimal.ZERO;
    
    @JsonProperty
    @Column
    private BigDecimal vat = BigDecimal.ZERO;
    
    @JsonProperty
    @Column(name = "total_amount")
    private BigDecimal totalAmount = BigDecimal.ZERO;
    
    @JsonProperty
    @Column
    private BigDecimal discount = BigDecimal.ZERO;
    
    @JsonProperty
    @Column
    private String remark;
    
    @JsonProperty
    @Column
    private Character status = 'A';
    
    @JsonProperty
    @Column(name = "customer_name")
    private String customerName = "";
    
    @JsonProperty
    @Column(name = "table_id")
    private Integer tableId;
    
    @JsonProperty
    @OneToMany(mappedBy = "order")
    private List<OrderDetail> orderDetails;
}
