package hcmut.restaurant.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "orders_detail")
public class OrderDetail implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @JsonProperty
    @Column
    private String remark;
    
    @JsonProperty
    @Column
    private Boolean status;
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "orders_id")
    private Order order;
    
    @JsonProperty
    @OneToOne
    @JoinColumn(name = "item_id")
    private Item item;
}
