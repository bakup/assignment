package hcmut.restaurant.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "item")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Item implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @JsonProperty
    @Column
    private String name;
    
    @JsonProperty
    @Column(name = "image_url")
    private String imageUrl;
    
    @JsonProperty
    @Column
    private String description;
    
    @JsonProperty
    @Column
    private BigDecimal price;
    
    @Column
    private Boolean status = true;
   
}
