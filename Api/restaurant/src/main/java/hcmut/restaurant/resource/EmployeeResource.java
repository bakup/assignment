package hcmut.restaurant.resource;

import hcmut.restaurant.entity.Role;
import hcmut.restaurant.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/admin/employee")
public class EmployeeResource {
    @Autowired
    private RoleRepository roleRepository;
    
    @PostMapping(value = "/create")
    public ResponseEntity<String> createEmployee() {
        Role r = new Role();
        r.setRoleName("TestRole1");
        r.setStatus('A');
        roleRepository.save(r);
        return ResponseEntity.ok().build();
    }
}
