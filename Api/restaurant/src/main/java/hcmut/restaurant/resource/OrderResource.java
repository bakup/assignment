package hcmut.restaurant.resource;

import hcmut.restaurant.dto.OrderDto;
import hcmut.restaurant.entity.Order;
import hcmut.restaurant.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderResource {
    
    @Autowired
    OrderService orderService;
    
    @PostMapping
    public ResponseEntity<Order> addOrder(@RequestBody OrderDto orderInfo) {
        Order order = orderService.addOrder(orderInfo);
        return new ResponseEntity<Order>(order, HttpStatus.OK);
    }
    
    @GetMapping
    public ResponseEntity<List<Order>> getAllOrder() {
        return null;
    }
}
