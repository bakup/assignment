package hcmut.restaurant.resource;

import hcmut.restaurant.entity.Item;
import hcmut.restaurant.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/admin/items")
public class ItemResource {
    
    @Autowired
    ItemRepository itemRepository;
    
    @PostMapping
    public ResponseEntity<Item> addItem(@RequestBody Item item) {
        itemRepository.save(item);
        return new ResponseEntity<Item>(item, HttpStatus.OK);
    }
    
    @GetMapping
    public ResponseEntity<List<Item>> getAllItem() {
        List<Item> items = itemRepository.findAll();
        return new ResponseEntity<List<Item>>(items, HttpStatus.OK);
    }
    
}
