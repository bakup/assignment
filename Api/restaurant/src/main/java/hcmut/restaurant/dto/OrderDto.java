package hcmut.restaurant.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto implements Serializable {
    
    @JsonProperty
    private List<Long> items = new ArrayList<>();
    
    @JsonProperty
    private BigDecimal vat = BigDecimal.ZERO;
    
    @JsonProperty
    private BigDecimal discount = BigDecimal.ZERO;
    
    @JsonProperty
    private Integer tableId = 1;
    
    @JsonProperty
    private String remark = "";
    
    @JsonProperty
    private String customer = "";
}
